import webpack from 'webpack'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import path from 'path'
import autoprefixer from 'autoprefixer'

export default {
  entry: './src/js/main.js',
  watch: true,
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.js',
    library: 'EntryPoint'
  },
  devtool: 'source-map',
  resolve: {
    alias: {
      scss: path.resolve('./src/scss'),
      js: path.resolve('./src/js'),
      static: path.resolve('./static')
    }
  },
  module: {
    loaders: [
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader!postcss-loader!sass-loader', // enabled sourcemaps,

        })
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015']
        }
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
            'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
            'image-webpack-loader?bypassOnDebug'
        ]
      },
      {
        test: /\.svg/,
        loader: 'svg-url-loader'
      },
      {
        test: /\.(eot|svg|ttf|ttc|woff|woff2|otf)$/,
        loader: 'file-loader?name=public/fonts/[name].[ext]'
      }
    ]
  },
  plugins: [
    // extracting styles
    new ExtractTextPlugin({
      filename: 'main.css',
      allChunks: true
    }),
    // makes a module available as a variable in every module
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: [
          autoprefixer('last 2 versions', 'ie 10')
        ]
       }
    })
  ]
}
