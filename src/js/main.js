import styles from '../scss/main.scss'

var formIsValid = true;


module.exports = {
    validateInput: function(element) {
        validateInput(element, validators.isElementEmpty);
    },
    submitForm: function(form) {
    	formIsValid = true;
        var nameElement = document.getElementById("name");
        var lastNameElement = document.getElementById("lastName");
        var messageElement = document.getElementById("message");

        validateInput(nameElement, validators.isElementEmpty);
        validateInput(lastNameElement, validators.isElementEmpty);
        validateInput(messageElement, validators.isElementEmpty);

        if(formIsValid){
        	alert('Form submitted succesfully')
        	return;
        }
        return false;
    }
};

function validateInput(element, validator) {
    if (validator(element)) {
        formIsValid = false;
        errorHandling.addErrorClass(element)
    } else {
        errorHandling.removeErrorClass(element);
    }
}


function addErrorClass(element) {
    element.classList.add('error');
}

function removeErrorClass(element) {
    element.classList.remove('error');
}

function isElementEmpty(element) {
    return !element.value;
}

//All validators should go here
var validators = {
    isElementEmpty: isElementEmpty
}

var errorHandling = {
    addErrorClass: addErrorClass,
    removeErrorClass: removeErrorClass
}